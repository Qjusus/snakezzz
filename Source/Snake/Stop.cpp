// Fill out your copyright notice in the Description page of Project Settings.


#include "Stop.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"



// Sets default values
AStop::AStop()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AStop::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AStop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

void AStop::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
			
		}
	}
}